from flask import Flask, request, render_template, redirect, url_for
import pymysql
import json
import flask_login

app = Flask(__name__)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

login_manager = flask_login.LoginManager()
login_manager.init_app(app)

#login_manager.login_view = "login"

class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(uname):
    user = User()
    user.id = uname
    return user

@app.route("/logout")
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return render_template("login.html",message = "Logout successful", color = "green")

@app.route('/')
@flask_login.login_required
def index():
    return render_template("login.html")

@app.route('/login',methods = ['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    username = request.form['uname']
    password = request.form['pwd']
    conn = pymysql.connect("localhost", "neva", "neva@123", "test")
    cur = conn.cursor()
    sql = "select pwd from neva_credentials where uname = '" + username + "'"
    cur.execute(sql)
    if(cur.rowcount != 0):
        pwd = cur.fetchone()
        pwd = pwd[0]
        if(pwd == password):
            current_user =  User()
            current_user.id = username
            flask_login.login_user(current_user)
            cur.close()
            conn.close()
            return redirect(url_for('home'))
        else:
            cur.close()
            conn.close()
            return render_template("login.html",message = "Invalid password!", color = "red")
    else:
        cur.close()
        conn.close()
        return render_template("login.html",message = "Invalid username", color = "red")

@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect('login')

@app.route('/home',methods = ['GET'])
@flask_login.login_required
def home():
    return render_template('home.html', user = flask_login.current_user.id)

@app.route('/getchat',methods = ['POST'])
@flask_login.login_required
def getchat():
    conn = pymysql.connect("localhost", "neva", "neva@123", "test")
    cur = conn.cursor()
    sql = "select frm,dest,message,cast(time as char) from neva_chat";
    cur.execute(sql)
    d =cur.fetchall()
    print(type(d))
    print(d)
    cur.close()
    conn.close()
    return json.dumps(d)


@app.route('/sayhi', methods = ['POST'])
@flask_login.login_required
def sayhi():
    conn = pymysql.connect("localhost", "neva", "neva@123", "test")
    cur = conn.cursor()
    if flask_login.current_user.id == 'bob':
        frm = 'Bob'
        dest = 'Alice'
    else:
        frm = 'Alice'
        dest = 'Bob'
    sql = "insert into neva_chat (frm,dest,message) values('" + frm + "','" + dest + "','Hi')"
    print(sql)
    cur.execute(sql)
    conn.commit()
    cur.close()
    conn.close()
    return("successfully sent hi to " + dest)

if __name__ == '__main__':
   app.run()
